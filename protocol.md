
Asus gaming keyboard TUF K7
===========================

Introduction
------------

ASUS TUF Gaming K7 is a optical mechanic keyboard. Its USB identifiers are:

- Vendor ID: 0B05
- Product ID: 18AA

This model is a 104/105 keys keyboard, equipped with an RGB led below each
key; each led is fully configurable; it also possible to enable some light
effects.

USB Interfaces overview
-----------------------

The USB protocol of this keyboard exports three interfaces. The first one is
the classic HID "keyboard" interface. The second interface is the one used to
control the led. The third interface exposes a more rich HID interfaces,
where are availables more features (like up to 120 simoultaneus keys,
mouse, multimedia keys); obviously some of these features are not available
with this hardware.

LED Control protocol
--------------------

The protocol is based to an exchange of hid frames send and received to/from
the second (logical) usb interface.
Each Frame is 64 bytes long. When the host send a frame, the keyboard always
replies with the echo of the received frames.

Each keys is identified by a numerical ID. Roughly the id are assigned
following the key phisical position. So ESC key as the ID 0, the tab KEY as
ID 2, CAPS LOCK has ID 3 (these keys are in the first column from the left).

Switch profile
--------------

The keyboard has four profiles. To switch to a new profile, the host sends
the following:

    0x51 0x00 0x00 0x00 <n> 0x00  ... [all zero up to 64 bytes]

Where <n> is the new profile:

    0 -> default profile
    1 -> profile #1
    2 -> profile #2
    3 -> profile #3

The keyboard will answer with an echo of this frame.


Set an effect to all keyboard
-----------------------------

**This command is not used by atufk7**.


    0x51 0x2c <effect data set>  ... [all zero up to 64 bytes]

Then the host sends an _end command_ frame

    0x50 0x55 0x00 0x00 ... [all zero up to 64 bytes]


Assign a static color to each key
---------------------------------

To assign a color to the keys, the host sends 10 frames: the first 9
frames contains the keys color, the 10th are the frame which ends the command.

The frames have the following structure

    1)  0x51 0xa8 0x00 0x01  BR RR GG BB  BR RR GG BB ....
    2)  0x51 0xa8 0x00 0x01  BR RR GG BB  BR RR GG BB ....

        [...]

    9)  0x51 0xa8 0x00 0x01  BR RR GG BB  BR RR GG BB ....
    10) 0x50 0x55 0x00 0x00 ... [all zero up to 64 bytes]

The first 9 frames, start with the prefix [0x51 0xa8 0x00 0x01], and then
there are the tuples of brightness (BR), red (RR), green (GG) blue (BB) one
for each key.
A frame has a size of 64 bytes. The first 4 are for the prefix, then there are
(64-4) / 4 = 15 tuplues for 15 keys. The first tuple is for the key with
ID = 0, the second tuple is for the key having ID = 1 and so on..

The last frame is the _end command_. It starts with 0x50, 0x55 followed by
zeros.

Assign an effect to a key
-------------------------

The following effects are available:

- ID=1 breather
- ID=2 color cycle
- ID=3 reactive
- ID=4 wave
- ID=5 ripple
- ID=6 starry night
- ID=7 quicksand
- ID=8 current
- ID=9 rain

It is possible to associate one of the above effect to each key. Each effect has
several parameters (speed, brigthness...). It is not possible to have the _same_
affect with different parameters.

To assign an effect to a key, the host sends:
1) a frames set that define which effect is enabled for which key.
2) a frames set that contain the effect data set (each effect and its
configuration)
3) the _end command_ [0x50 0x55 0x00...]


The first frames set is composed by three frames:

    1)  0x51 0xa0 0x00 0x00 0x00 0x07 0x00 0x00 <eff-key 0> ... <eff-key-55>
    2)  0x51 0xa0 0x00 0x00 0x07 0x07 0x00 0x00 <eff-key 56> ... <eff-key-111>
    3)  0x51 0xa0 0x00 0x00 0x0e 0x0e 0x00 0x00 <eff-key 112> ... <eff-key-168>

Where the **<eff-key NN>** is the effect-ID associated to the key **NN**.

The second frames set is composed by further 26 frames:

    4)  0x51 0x0a 0x00 0x01 <effect data set> <effect data set>  .... [up to 64 bytes]
    5)  0x51 0x0a 0x00 0x01 <effect data set> <effect data set>  .... [up to 64 bytes]
    [...]
    29) 0x51 0x0a 0x00 0x01 <effect data set> <effect data set>  ....  [up to 64 bytes]

Finally, there is the _end command_ frame.

    30) 0x50 0x55 0x00 0x00 ... [all zero up to 64 bytes]

The **<effect data set>** is a sequence of 15 bytes, where the first byte
identifies the effect. As default (no effect), the sequence has the form:

    0xff 0x00 0xff 0xff 0x00 0xff 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00

So the first three frames associate an effect to a key. The next 26 frames, are
used to store the effect configuration.
It must be point out that 26 frames to store the effect configurato would allow
up to 26*4 = 104 effects; hoever because the effect are a lot less (in the order
of 10) most of the 26 frames are unused.

Effect breather  (ID=1)
-----------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0xff <mode> 0xff <R1> <G1> <B1> <R2> <G2> <B2> 0x00 0x00 0x00

Where
- <id>  is 1
- <speed> is
  - 2 -> fast
  - 4 -> normal
  - 7 -> slow
- <brightness> value of brightness (0, 25, 50, 100)
- <mode>
  - 0 -> one color
  - 16 -> two color
  - 17 -> random color
- R1, G1, B1, are the color #1 (only mode one color / two color)
- R2, G2, B2, are the color #2 (only mode two color)

Effect color cycle  (ID=2)
--------------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0xff 0x00 0xff 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00

Where
- <id>  is 2
- <speed> is
  - 1 -> fast
  - 3 -> normal
  - 6 -> slow
- <brightness> value of brightness (0, 25, 50, 100)

Effect reactive  (ID=3)
-----------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0xff <mode> 0xff <R1> <G1> <B1> <R2> <G2> <B2> 0x00 0x00 0x00

Where
- <id>  is 3
- <speed> is
  - 5 -> fast
  - 9 -> normal
  - 14 -> slow
- <brightness> value of brightness (0, 25, 50, 100)
- <mode>
  - 0 -> one color
  - 16 -> two color
  - 17 -> random color
- R1, G1, B1, are the color #1 (only mode one color / two color)
- R2, G2, B2, are the color #2 (only mode two color)


Effect wave  (ID=4)
-----------------------

An additional frame is inserted before the frame which starts with  **0x51 0x01 0x00 0x01**; the frame has the following structure:

    0x51 0x91 0x04 0x01 <nr-col> <pos> <rr> <gg> <bb> <pos> <rr> <gg> <bb>  ... 0x00 ...

Where
- <nr-col> is the repeation number of the tuple (<pos> <rr> <gg> <bb>)
- <pos> is the position of the color in the range 0..64
- <rr> <gg> <bb> is a color

The **<effect data set>** has the following structure

    <id> <speed> <brightness> <dir>+<thk> 0x00 0xff 0x00 ... 0x00

Where
- <id>  is 4
- <speed> is
  - 5 -> fast
  - 10 -> normal
  - 15 -> slow
- <thk> is thickness 2-> thick, 1 -> medium, slim = 0
- <dir> is direction : up 60, down 20, 40 left, right 00, 80 left-right, left-bottom 30,
                      right bottom 10,  upper-right 70, upper-left 50
- <brightness> value of brightness (0, 25, 50, 100)

Effect ripple  (ID=5)
-----------------------

An additional frame is inserted before the frame which starts with  **0x51 0x01 0x00 0x01**; the frame has the following structure:

    0x51 0x91 0x05 0x01 <nr-col> <pos> <rr> <gg> <bb> <pos> <rr> <gg> <bb>  ... 0x00 ...

Where
- <nr-col> is the repeation number of the tuple (<pos> <rr> <gg> <bb>)
- <pos> is the position of the color in the range 0..64
- <rr> <gg> <bb> is a color

The **<effect data set>** has the following structure

    <id> <speed> <brightness> <thk> 0x00 0xff 0x00 ... 0x00

Where
- <id>  is 5
- <speed> is
  - 5 -> fast
  - 10 -> normal
  - 15 -> slow

- <thk> is thickness 62-> thick, 61 -> medium, slim = 60
- <brightness> value of brightness (0, 25, 50, 100)



Effect starry night  (ID=6)
---------------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0x02 <mode1> 0xff <R1> <G1> <B1> <R2> <G2> <B2> <R3> <G3> <B3>

Where
- <id>  is 6
- <speed> is
  - 2 -> fast
  - 5 -> normal
  - 8 -> slow
- <brightness> value of brightness (0, 25, 50, 100)
- <mode1> -> mode of the star
  - 0 -> one color
  - 16 -> two color
  - 17 -> random color
- R1, G1, B1, are the color #1 (only mode one color / two color)
- R2, G2, B2, are the color #2 (only mode two color)
- R3, G3, B3, are the color #3 (color of the "sky")


Effect quicksand  (ID=7)
------------------------

An additional frame is inserted before the frame which starts with  **0x51 0x01 0x00 0x01**; the frame has the following structure:

    0x51 0x91 0x07 0x01 <rr1> <gg1> <bb1> ... <rr6> <gg6> <bb6>  ... 0x00 ...

Where
- <rr> <gg> <bb> is a color

In this frame are stored the 6 colors used by this effect

The **<effect data set>** has the following structure

    <id> <speed> <brightness> <dir> 0x00 0xff 0x00 ... 0x00

Where
- <id>  is 7
- <speed> is
  - 5 -> fast
  - 10 -> normal
  - 15 -> slow
- <dir> is direction : down 0x2f, up 6f
- <brightness> value of brightness (0, 25, 50, 100)


Effect current  (ID=8)
----------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0x60 <mode1> 0xff <R1> <G1> <B1> <R2> <G2> <B2> <R3> <G3> <B3>

Where
- <id>  is 8
- <speed> is
  - 7 -> fast
  - 11 -> normal
  - 15 -> slow
- <brightness> value of brightness (0, 25, 50, 100)
- <mode1> -> mode of the "star"
  - 0 -> one color
  - 16 -> two color
  - 17 -> random color
- R1, G1, B1, are the color #1 (only mode one color / two color)
- R2, G2, B2, are the color #2 (only mode two color)
- R3, G3, B3, are the color #3 (color of the "sky")

Effect rain  (ID=9)
-------------------

The **<effect data set>** has the following structure

    <id> <speed> <brightness> 0x22 <mode1> 0xff <R1> <G1> <B1> <R2> <G2> <B2> <R3> <G3> <B3>

Where
- <id>  is 8
- <speed> is
  - 6 -> fast
  - 9 -> normal
  - 13 -> slow
- <brightness> value of brightness (0, 25, 50, 100)
- <mode1> -> mode of the "star"
  - 0 -> one color
  - 16 -> two color
  - 17 -> random color
- R1, G1, B1, are the color #1 (only mode one color / two color)
- R2, G2, B2, are the color #2 (only mode two color)
- R3, G3, B3, are the color #3 (color of the "sky")



Keys identifier
---------------
Below an "ascii art" which associate a numerical ID to each key. Pay attention
that the layout is the "us" one with the addition of the '<' key, which is
present only in the 105 keys (the us layout is 104 keys).
Another differences between the layouts are the shape of the 'RETURN' key: it
can be large and low or narrow and toll. Depending on that the '\' kay ma be
on the left of the 'ENTER' key or on the top.
Pay attention that each keys has a numerical ID. But some numerical ID are not
associated to any key.
For example the _last_ key is the KPRETURN (the return on the numerical pad),
which has the ID = 122. However the ID 117 is not associatted to any key.

    ESC   F1  F2  F3  F4     F5  F6  F7 F8     F9  F10  F11 F12
    0     8   16  24  32     40  48  56 64     72  80   46  47

    `    1   2   3   4   5   6   7   8   9   0   -   =   BACKSPACE
    1    6   7   9   17  25  33  41  49  57  65  73  81   55

    TAB    Q   W   E   R   T   Y   U   I   O   P   [   ]    ENTER
    2      14  15  10  18  26  34  42  50  58  66  74  82   63

    CAPS.L   A   S   D   F   G   H   J   K   L   ;   '   \
    3        22  23  11  19  27  35  43  51  59  67  75  83

    LSHIFT  <   Z   X   C   V   B   N   M   ,   .   /   RSHIFT
    4       30  31  12  13  20  28  36  44  52  60  68  84

    LCTRL LWIN LALT           SPACE         LALT  FN  RWIN  RCTRL
    5     38   39             29            39    69  77    85




    RSYS  SCROLLL  PAUSE
    88    89       90

    INS   HOME     PGUP        NUML  KPSLASH  KPSTAR   KPMINUS
    70    78       94          100   108      116      124

    DEL   END      PGDOWN      KP7   KP8      KP9      KPPLUS
    71    79       95          96    104      112      120

                               KP4   KP5      KP6
                               97    105      113

          UP                   KP1   KP2      KP3      KPENTER
          87                   98    106      114      122

    LEFT  DOWN     RIGHT       KP0            KPDEL
    86    93       92          107            115
