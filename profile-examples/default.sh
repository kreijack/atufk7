sudo python3 atufk7.py \
	set-profile 1 \
	set-color ALL x64202020 \
	set-color ESC x640000ff \
	set-color UP,DOWN,LEFT,RIGHT,DEL,BACKSPACE x19800000 \
	set-color LSHIFT,RSHIFT,LCTRL,RCTRL,LALT,RALT,LWIN x19008000 \
	set-color RWIN,FN,TAB x19008000 \
	set-color CAPSLOCK,NUMLOCK,SCROLLLOCK x640000ff \
	set-breather-effect SPACE double:fast:100:x800080:x008000 \
	set-color F1,F2,F3,F4,F9,F10,F11,F12 25,255,0,0 \
	set-color F5,F6,F7,F8 25,0,0,255
