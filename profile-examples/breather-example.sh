sudo python3 atufk7.py \
	set-default-color x64202020 \
	set-color ESC x640000ff \
	set-color UP,DOWN,LEFT,RIGHT,DEL,BACKSPACE x32800000 \
	set-color LALT,RALT,LWIN,RWIN,FN,CAPSLOCK,TAB x32008000 \
	set-color LSHIFT,RSHIFT,LCTRL,RCTRL x32008000 \
	set-breather-effect SPACE double:fast:100:x800080:x008000 \
	set-color-cycle-effect NUMLOCK slow:100 \
	set-reactive-effect F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12 \
		double:normal:100:x800080:x202020
