atufk7
======

atufk7 is a python script to configure the ASUS TUF Gaming K7 mechanical
keyboard.
This script configure the led below each key and the light effects.

Below the profile-examples/ directory there are some scripts which configure
several effects and lights layout.

This script requires root profileges because it handles the HID frames of the
USB protocol.

To show the help, run the atufk7 command followed by 'help' subcommand

    $ python3 atufk7.py help

    usage: atufk7 cmd [cmd [cmd ... ]]
        where cmd is:
        set-default-color <color>             set a default colot for all keys

        set-profile <nr>                      set the active profile (nr=0..3)
        [...]


To set the blue color to all the keys

    $ sudo python3 atufk7.py set-color ALL 100,0,0,255

Where:
- ALL          mean all keys
- 100          is the brightness (in the range 0..100)
- 0,0,255      is the RGB value of the blue color

To set the white as default color, blue to the ESC key, and the 'breather'
effect to the space bar

    $ sudo python3 atufk7.py \
    >    set-color ALL 100,255,255,255 \
    >    set-color ESC 100,0,0,255 \
    >    set-breather-effect SPACE double:fast:100:x800080:x008000


To get a list of key names

    $ python3 atufk7.py help-keys
    The available keys name are:
        - 0 ... 9         for the digits
        - A ... Z         for the simple letter
        - APOSTROPHE
        - BACKSLASH
    [...]



The full help is showed below

        usage: atufk7 cmd [cmd [cmd ... ]]
        where cmd is:
            set-default-color <color>             set a default colot for all keys

            set-profile <nr>                      set the active profile (nr=0..3)
                                                  nr=0 -> default profile

            set-color <key>[,<key>..] <color>     set the color of specific 'key[s]'

            set-breather-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]]
                <mode>          single|double|random
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <c1>,<c2>       color

            set-reactive-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]]
                <mode>          single|double|random
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <c1>,<c2>       color

            set-color-cycle-effect <key>[,<key>..] <speed>:<br>
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100

            set-starry-night-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
                <mode>          single|double|random
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <c1>,<c2>       color
                <c3>            default color (default: 0,0,0)

            set-current-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
                <mode>          single|double|random
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <c1>,<c2>       color
                <c3>            default color (default: 0,0,0)

            set-rain-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
                <mode>          single|double|random
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <c1>,<c2>       color
                <c3>            default color (default: 0,0,0)

            set-quicksand-effect <key>[,<key>..] <speed>:<br>:<dir>[c1:c2:..:c6]
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <dir>           up|down
                <c1>,<c2>..<c6> 6 colors (or default if omitted)

            set-wave-effect <key>[,<key>..] <speed>:<br>:<dir>:<thk>[p1,c1:..:pn,cn]
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <dir>           direction: right,bottom-right,bottom,bottom-left,
                                left,upper-left,upper,upper-right
                <thk>           thickness: thick,normal,slim
                <p1,c1>..<pn,cn> colors with position (or default if omitted)
                                position between [0..100]

            set-ripple-effect <key>[,<key>..] <speed>:<br>:<thk>[p1,c1:..:pn,cn]
                <speed>         slow|normal|fast
                <br>            brightness: 0|25|50|75|100
                <thk>           thickness: thick,normal,slim
                <p1,c1>..<pn,cn> colors with position (or default if omitted)
                                position between [0..100]

            help|--help         show this page
            help-keys           show the keys name
            show-keys           show one key at time to know the name

            a color may be expressed as:
                xbrrrggbb          where br rr gg bb are in hex format
                br,rr,gg,bb        where br rr gg bb are in decimal format

