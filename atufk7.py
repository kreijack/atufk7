# Copyright (C) 2020 Goffredo Baroncelli
#
# This file is part of atufk7.
#
# atufk7 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# atufk7 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with atufk7.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import json
import pprint
import array
import time
import sys

import usb.core
import usb.util




class AsusKbd:
    num_keys = int(9*(64-4)/4)

    def __init__(self):
        self._debug = True
        self.num_keys = AsusKbd.num_keys
        self._dev = None

        self._dev = usb.core.find(idVendor=0x0b05, idProduct=0x18aa)

        # was it found?
        if self._dev is None:
            raise ValueError('Device not found')

         # don't configure: the keyboard sometime doesn't like it
        #self._dev.set_configuration()

        if self._dev.is_kernel_driver_active(1):
            self._dev.detach_kernel_driver(1)

        # get an endpoint instance
        cfg = self._dev.get_active_configuration()
        # interface nr 1
        intf = cfg[(1,0)]

        ep = usb.util.find_descriptor(
            intf,
            # match the first OUT endpoint
            custom_match = \
            lambda e: \
                usb.util.endpoint_direction(e.bEndpointAddress) == \
                usb.util.ENDPOINT_OUT)

        epi = usb.util.find_descriptor(
            intf,
            # match the first OUT endpoint
            custom_match = \
            lambda e: \
                usb.util.endpoint_direction(e.bEndpointAddress) == \
                usb.util.ENDPOINT_IN)


        assert ep is not None
        assert epi is not None

        self._ep = ep
        self._epi = epi

    def set_debug(self, d):
        self._debug = d

    def _print_data(self, d, dir_=">"):
        if not self._debug:
            return

        print((dir_ + "[" + ",".join("%02x"%(x) for x in d))[:70]+"...]")

    def _send_data(self, data_to_write):
        for line in data_to_write:
            s = array.array('B', line)

            # write the data
            self._print_data(s)
            assert(len(s) == 64)
            self._ep.write(s)

            # read back the answer
            d = self._epi.read(len(line), timeout=10000)

            if d != s:
                # the other answers are always equal to the sent message
                print("Want: ", s)
                print("Got:  ", d)
                assert(False)

            #time.sleep(0.02)

    def get_layout(self):
        # from OpenRGB

        d = [0x00] * 64
        d[0] = 0x12
        d[1] = 0x12

        self._print_data(d)
        assert(len(d) == 64)
        self._ep.write(d)

        ret = self._epi.read(64, timeout=10000)
        assert(len(ret) == 64)
        self._print_data(ret, "<")

        return ret[3] * 100 + ret[4]

    def get_version(self):
        # from OpenRGB

        d = [0x00] * 64
        d[0] = 0x12

        self._print_data(d)
        assert(len(d) == 64)
        self._ep.write(d)

        ret = self._epi.read(64, timeout=10000)
        assert(len(ret) == 64)
        self._print_data(ret, "<")

        return "%02X.%02X.%02X"%(ret[4], ret[5], ret[6])

    def set_keys_color(self, keys):
        add_effect = False
        add_static = False

        for i in range(self.num_keys):
            if i < len(keys):
                (mode, c) = keys[i]
                if mode == 'static':
                    add_static = True
                elif mode == '':
                    assert(False)
                else:
                    add_effect = True

        if add_static:
            self._do_set_static_color(keys)

        if add_static or add_effect:

           self._send_data(self._build_impacted_keys(keys))
           self._send_data(self._build_effects(keys))

           self._send_data([[ 0x50, 0x55, 0x00, 0x00 ] + [0x00] * 60])

    def _do_set_static_color(self, keys):
        # the key color are sent in the following format:
        # 9 blocks of 64 bytes format:
        # byte 0..3    0x51 0xa8 0x00 0x01
        # byte 4..63, grouped in 4 bytes one for each key: BR RR GG BB
        #   where BR -> brightness  (0..100)
        #   where RR -> RED, GG -> Green, BB -> Blue (0..255)
        # after 9 blocks there is another one
        # byte 0..2    0x50 0x55 0x00 0x00
        # byte 4..63, all 0
        blocks = []
        #blocks.append([0x12, 0x12] + [0x00 for x in range(62)])

        #print("keys=", keys)

        line0 = [0x51, 0xa8, 0x00, 0x01]
        line = line0.copy()

        for i in range(self.num_keys):
            if i < len(keys):
                (mode, c) = keys[i]
                if mode == 'static':
                    br, rr, gg, bb = c
                    if br == 0:
                        # this really black all the keys
                        br = 25
                        rr = bb = gg = 0
                else:
                    br, rr, gg, bb = 0xfe , 0xfe,  0xfe, 0xfe
            else:
                br, rr, gg, bb = 0xfe , 0xfe,  0xfe, 0xfe

            line.extend((br, rr, gg, bb))
            assert(len(line) <= 64)

            if len(line) == 64:
                blocks.append(line)
                line = line0.copy()

        blocks.append([ 0x50, 0x55, 0x00, 0x00 ] + [0x00] * 60 )

        self._send_data(blocks)

    def set_profile(self, profile):
        assert(profile >= 0 and profile <= 3)
        self._send_data([[0x51, 0x00, 0x00, 0x00, profile] + [0x00] * 59])
        self._send_data([[0x50, 0x55, 0x00, 0x00 ] + [0x00] * 60])

    def _build_effects(self, keys):
        additional_blocks = []
        blocks = [[0x51, 0xa0, 0, 1] +
                    [255, 0, 255, 255, 0, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0] * 4
                ] * 26

        c = 0
        processed = set()
        for i in range(self.num_keys):
            (mode, arg) = keys[i]

            # process an effect only one time
            if mode in processed:
                continue
            processed.add(mode)

            if mode == 'static':
                pass
            elif mode == 'breather':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                c1, c2, speed, m, br = arg
                blocks[row][col+0] = 1  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 255
                blocks[row][col+4] = m  # mode
                blocks[row][col+5] = 255
                for j in range(3):
                    blocks[row][col+6+j] = c1[j]
                    blocks[row][col+9+j] = c2[j]
                c += 1
            elif mode == 'color-cycle':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                speed, br = arg
                blocks[row][col+0] = 2  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 255
                blocks[row][col+4] = 0
                blocks[row][col+5] = 255

                c += 1
            elif mode == 'reactive':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                c1, c2, speed, m, br = arg
                blocks[row][col+0] = 3  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 255
                blocks[row][col+4] = m  # type
                blocks[row][col+5] = 255
                for j in range(3):
                    blocks[row][col+6+j] = c1[j]
                    blocks[row][col+9+j] = c2[j]
                c += 1

            elif mode == 'starry-night':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                c1, c2, c3, speed, m, br = arg
                blocks[row][col+0] = 6  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 0x02   # ?? big thickness
                blocks[row][col+4] = m  # type
                blocks[row][col+5] = 255
                for j in range(3):
                    blocks[row][col + 6  + j] = c1[j]
                    blocks[row][col + 9  + j] = c2[j]
                    blocks[row][col + 12 + j] = c3[j]
                c += 1

            elif mode == 'current':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                c1, c2, c3, speed, m, br = arg
                blocks[row][col+0] = 8  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 0x22   # ?? big thickness
                blocks[row][col+4] = m  # type
                blocks[row][col+5] = 255
                for j in range(3):
                    blocks[row][col + 6  + j] = c1[j]
                    blocks[row][col + 9  + j] = c2[j]
                    blocks[row][col + 12 + j] = c3[j]
                c += 1

            elif mode == 'rain':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                c1, c2, c3, speed, m, br = arg
                blocks[row][col+0] = 9  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = 0x60   # ?? big thickness
                blocks[row][col+4] = m  # type
                blocks[row][col+5] = 255
                for j in range(3):
                    blocks[row][col + 6  + j] = c1[j]
                    blocks[row][col + 9  + j] = c2[j]
                    blocks[row][col + 12 + j] = c3[j]
                c += 1

            elif mode == 'quicksand':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                cls, speed, br, d = arg
                blocks[row][col+0] = 7  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = d
                blocks[row][col+4] = 0
                blocks[row][col+5] = 255
                c += 1

                b = [ 0x51, 0x91, 0x07, 0x01 ]
                for c in cls:
                    b.extend(c)

                while len(b) < 64:
                    b.append(0x00)

                additional_blocks.append(b)

            elif mode == 'wave':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                cls, speed, br, d, thickness = arg
                blocks[row][col+0] = 4  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = d * 0x10 + thickness
                blocks[row][col+4] = 0
                blocks[row][col+5] = 255
                c += 1

                b = [ 0x51, 0x91, 0x04, 0x01, len(cls) ]
                for c in cls:
                    b.extend(c)

                while len(b) < 64:
                    b.append(0x00)

                additional_blocks.append(b)

            elif mode == 'ripple':
                row = int(c /4)
                col = (c % 4) * 15 + 4

                cls, speed, br, thickness = arg
                blocks[row][col+0] = 5  # type
                blocks[row][col+1] = speed  #
                blocks[row][col+2] = br  # brightness
                blocks[row][col+3] = thickness
                blocks[row][col+4] = 0
                blocks[row][col+5] = 255
                c += 1

                b = [ 0x51, 0x91, 0x05, 0x01, len(cls) ]
                for c in cls:
                    b.extend(c)

                while len(b) < 64:
                    b.append(0x00)

                additional_blocks.append(b)

            else:
                print("Unknown mode '%s'"%(mode))
                assert(False)
        return additional_blocks + blocks


    def _build_impacted_keys(self, keys):
        blocks = [[], [], []]
        blocks[0] = [0x51, 0xa0, 0x0, 0x0, 0x00, 0x07, 0x0, 0x0] + [0xfe] * 56
        blocks[1] = [0x51, 0xa0, 0x0, 0x0, 0x07, 0x07, 0x0, 0x0] + [0xfe] * 56
        blocks[2] = [0x51, 0xa0, 0x0, 0x0, 0x0d, 0x02, 0x0, 0x0] + [0xfe] * 56
        for i in range(self.num_keys):
            (mode, arg) = keys[i]
            if mode == 'static':
                blocks[int(i/56)][8 + i %56] = 0
            elif mode == 'breather':
                blocks[int(i/56)][8 + i %56] = 1
            elif mode == 'color-cycle':
                blocks[int(i/56)][8 + i %56] = 2
            elif mode == 'reactive':
                blocks[int(i/56)][8 + i %56] = 3
            elif mode == 'wave':
                blocks[int(i/56)][8 + i %56] = 4
            elif mode == 'ripple':
                blocks[int(i/56)][8 + i %56] = 5
            elif mode == 'starry-night':
                blocks[int(i/56)][8 + i %56] = 6
            elif mode == 'quicksand':
                blocks[int(i/56)][8 + i %56] = 7
            elif mode == 'current':
                blocks[int(i/56)][8 + i %56] = 8
            elif mode == 'rain':
                blocks[int(i/56)][8 + i %56] = 9
            else:
                print("Unknown mode '%s'"%(mode))
                assert(False)

        return blocks

keyboard  = [(0, 'ESC'), (1, "GRAVE"), (2, 'TAB'), (3, 'CAPSLOCK'),
    (4, 'LSHIFT'), (5, 'LCTRL'), (6, '1'), (7, '2'), (8, 'F1'), (9, '3'),
    (10, 'E'), (11, 'D'), (12, 'X'), (13, 'C'),
    (14, 'Q'), (15, 'W'), (16, 'F2'), (17, '4'), (18, 'R'), (19, 'F'),
    (20, 'V'), (21, ''), (22, 'A'), (23, 'S'), (24, 'F3'), (25, '5'),
    (26, 'T'), (27, 'G'), (28, 'B'), (29, 'SPACE'),
    (30, 'LESSTHAN'), (31, 'Z'), (32, 'F4'), (33, '6'),
    (34, 'Y'), (35, 'H'), (36, 'N'), (37, ''),
    (38, 'LWIN'), (39, 'LALT'), (40, 'F5'), (41, '7'), (42, 'U'),
    (43, 'J'), (44, 'M'), (45, ''), (46, 'F11'), (47, 'F12'), (48, 'F6'),
    (49, '8'), (50, 'I'), (51, 'K'), (52, 'COMMA'), (53, ''), (54, ''),
    (55, 'BACKSPACE'), (56, 'F7'), (57, '9'), (58, 'O'), (59, 'L'),
    (60, 'DOT'), (61, 'RALT'), (62, ''), (63, 'ENTER'), (64, 'F8'), (65, '0'),
    (66, 'P'), (67, 'SEMICOLON'), (68, 'SLASH'),
    (69, 'FN'), (70, 'INS'), (71, 'DEL'),
    (72, 'F9'), (73, "MINUS"), (74, 'LEFTBRACE'),
    (75, "APOSTROPHE"), (76, ''), (77, 'RWIN'),
    (78, 'HOME'), (79, 'END'), (80, 'F10'), (81, 'RIGHTBRACE'),
    (82, 'PLUS'), (83, 'BACKSLASH'),
    (84, 'RSHIFT'), (85, 'RCTRL'), (86, 'LEFT'), (87, 'UP'), (88, 'SYSRQ'),
    (89, 'SCROLLLOCK'), (90, 'PAUSE'), (91, ''), (92, 'RIGHT'), (93, 'DOWN'),
    (94, 'PGUP'), (95, 'PGDOWN'), (96, 'KP7'), (97, 'KP4'), (98, 'KP1'),
    (99, ''), (100, 'NUMLOCK'), (101, ''), (102, ''), (103, ''), (104, 'KP8'),
    (105, 'KP5'), (106, 'KP2'), (107, 'KP0'), (108, 'KPSLASH'), (109, ''),
    (110, ''), (111, ''), (112, 'KP9'), (113, 'KP6'), (114, 'KP3'),
    (115, 'KPDEL'),
    (116, 'KPASTERISK'), (117, ''), (118, ''), (119, ''),
    (120, 'KPPLUS'), (121, ''),
    (122, 'KPENTER'), (123, ''), (124, 'KPMINUS'), (125, ''), (126, ''),
    (127, ''), (128, ''), (129, ''), (130, ''), (131, ''), (132, ''),
    (133, ''), (134, '')]

class ParserError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class ParseCMDLine:
    def __init__(self, args):
        self._keys = None
        self._args = args
        self._profile = None
        self._get_version = False
        self._get_layout = False
        self._debug = False

    def _init_keys(self):
        self._keys = [('static', (0, 0,0,0))] * AsusKbd.num_keys

    def _set_default_color(self, color):
        if not self._keys:
            self._init_keys()
        color = self._parse_brightness_and_color(color)
        for (nr, key) in keyboard:
            if key != '':
                self._keys[nr] = ('static', color)

    def _set_color(self, keys, color):
        if not self._keys:
            self._init_keys()
        keys = set(keys.split(","))
        color = self._parse_brightness_and_color(color)
        for (nr, key2) in keyboard:
            if key2 in keys or "ALL" in keys:
                self._keys[nr] = ('static', color)
                keys.discard(key2)

        keys.discard("ALL")
        if len(keys):
            print("Can't find the following keys: %r"%(keys))
            assert(False)

    def _parse_any_of(self, v, d):
        if v in d:
            return d[v]

        raise ParserError("Parameter values '%s' not allowed; valid values: %s"%(
            v, ",".join(d.keys())))

    def _parse_num(self, v, min_, max_):
        try:
            v = int(v)
        except:
            raise ParserError("Cannot convert parameter '%s' to an integer"%(
                str(v)))

        if v >= min_ and v <= max_:
            return v

        raise ParserError("Parameter values '%d' not in range [%d..%d]"%(
            v, min_, max_))

    def _set_breather_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 3:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        c1 = (0, 0, 0)
        c2 = (0, 0, 0)

        (ncolmin, mode) = self._parse_any_of(args[0],
            {"random": (0, 0), "single": (1, 16), "double": (2, 17)})

        if len(args) > 3 + ncolmin:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        if ncolmin >= 1:
            c1 = self._parse_color(args[3])
        if ncolmin == 2:
            c2 = self._parse_color(args[4])

        speed = self._parse_any_of(args[1], {"slow":7, "normal":4, "fast": 2})

        br = self._parse_num(args[2], 0, 100)

        self._set_keys(keys, ('breather', (c1, c2, speed, mode, br)))


    def _set_reactive_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 3:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        c1 = (0, 0, 0)
        c2 = (0, 0, 0)

        (ncolmin, mode) = self._parse_any_of(args[0],
            {"random": (0, 0), "single": (1, 16), "double": (2, 17)})

        if len(args) > 3 + ncolmin:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        if ncolmin >= 1:
            c1 = self._parse_color(args[3])
        if ncolmin == 2:
            c2 = self._parse_color(args[4])

        speed = self._parse_any_of(args[1], {"slow":14, "normal":9, "fast": 5})

        br = self._parse_num(args[2], 0, 100)

        self._set_keys(keys, ('reactive', (c1, c2, speed, mode, br)))

    def _set_starry_night_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 3:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        c1 = (0, 0, 0)
        c2 = (0, 0, 0)
        c3 = (0, 0, 0)

        (ncolmin, mode) = self._parse_any_of(args[0],
            {"random": (0, 0), "single": (1, 16), "double": (2, 17)})

        if len(args) > 3 + ncolmin + 1:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        if len(args) == 3 + ncolmin + 1:
            c3 = self._parse_color(args[-1])

        if ncolmin >= 1:
            c1 = self._parse_color(args[3])
        if ncolmin == 2:
            c2 = self._parse_color(args[4])

        speed = self._parse_any_of(args[1], {"slow":8, "normal":5, "fast": 2})

        br = self._parse_num(args[2], 0, 100)

        self._set_keys(keys, ('starry-night', (c1, c2, c3, speed, mode, br)))

    def _set_current_effect(self, keys, args0):
        args = args0.split(":")
        c3 = (0, 0, 0)
        if len(args) < 3:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        c1 = (0, 0, 0)
        c2 = (0, 0, 0)
        c3 = (0, 0, 0)

        (ncolmin, mode) = self._parse_any_of(args[0],
            {"random": (0, 0), "single": (1, 16), "double": (2, 17)})

        if len(args) > 3 + ncolmin + 1:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        if len(args) == 3 + ncolmin + 1:
            c3 = self._parse_color(args[-1])

        if ncolmin >= 1:
            c1 = self._parse_color(args[3])
        if ncolmin == 2:
            c2 = self._parse_color(args[4])

        speed = self._parse_any_of(args[1], {"slow":15, "normal":11, "fast": 7})

        br = self._parse_num(args[2], 0, 100)

        self._set_keys(keys, ('current', (c1, c2, c3, speed, mode, br)))

    def _set_rain_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 3:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        c1 = (0, 0, 0)
        c2 = (0, 0, 0)
        c3 = (0, 0, 0)

        (ncolmin, mode) = self._parse_any_of(args[0],
            {"random": (0, 0), "single": (1, 16), "double": (2, 17)})

        if len(args) > 3 + ncolmin + 1:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        if len(args) == 3 + ncolmin + 1:
            c3 = self._parse_color(args[-1])

        if ncolmin >= 1:
            c1 = self._parse_color(args[3])
        if ncolmin == 2:
            c1 = self._parse_color(args[4])

        speed = self._parse_any_of(args[1], {"slow":13, "normal":9, "fast": 6})

        br = self._parse_num(args[2], 0, 100)

        self._set_keys(keys, ('rain', (c1, c2, c3, speed, mode, br)))

    def _set_quicksand_effect(self, keys, args0):

        args = args0.split(":")

        if len(args) != 3 and len(args) != 9:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        speed = self._parse_any_of(args[0],
            {"slow":15, "normal":10, "fast": 5})

        br = self._parse_num(args[1], 0, 100)

        d = self._parse_any_of(args[2], {"up": 0x6f, "down":0x2f})

        cls = self._parse_colors(args[3:])

        self._set_keys(keys, ('quicksand', (cls, speed, br, d)))

    def _set_wave_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 4 or len(args) > 11:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        speed = self._parse_any_of(args[0],
            {"slow":15, "normal":10, "fast": 5})

        br = self._parse_num(args[1], 0, 100)

        d = self._parse_any_of(args[2], {
            "right": 0,
            "bottom-right": 1,
            "bottom": 2,
            "bottom-left": 3,
            "left": 4,
            "upper-left": 5,
            "upper": 6,
            "upper-right": 7,
        })

        thickness = self._parse_any_of(args[3], {
            "thick":2, "normal":1, "slim":0
        })

        cls = self._parse_colors(args[4:])

        self._set_keys(keys, ('wave', (cls, speed, br, d, thickness)))

    def _set_ripple_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) < 3 or len(args) > 10:
            raise ParserError("Error in number of parameter in argument '%s'"%(
                args0))

        speed = self._parse_any_of(args[0],
            {"slow":15, "normal":10, "fast": 5})

        br = self._parse_num(args[1], 0, 100)

        thickness = self._parse_any_of(args[2], {
            "thick":62, "normal":61, "slim":60
        })

        cls = self._parse_colors(args[3:])

        self._set_keys(keys, ('ripple', (cls, speed, br, thickness)))

    def _set_color_cycle_effect(self, keys, args0):
        args = args0.split(":")

        if len(args) != 2:
            raise ParserError("Parameter '%s' must have only two options"%(args0))

        speed = self._parse_any_of(args[0], {"slow":6, "normal":3, "fast": 1})
        br = self._parse_num(args[1], 0, 100)

        self._set_keys(keys, ('color-cycle', (speed, br)))

    def _set_keys(self, keys, value):
        if not self._keys:
            self._init_keys()

        keys = set(keys.split(","))
        for (nr, key2) in keyboard:
            if key2 in keys or "ALL" in keys:
                self._keys[nr] = value
                keys.discard(key2)

        keys.discard("ALL")
        if len(keys):
            raise ParserError("Can't find the following keys: %s"%(
                ", ".join(keys)))

    def _parse_colors(self, args):
        if len(args) == 0:
            return [(  0, 0xff, 0x00, 0x10),
               ( 20, 0xe8, 0x00, 0xff),
               ( 40, 0x00, 0x02, 0xff),
               ( 60, 0x00, 0xff, 0xf9),
               ( 80, 0x00, 0xff, 0x08),
               (100, 0x08, 0xe9, 0xff)]

        if len(args) >7:
            raise ParserError("Too much number of colors in argument '%s'"%(args0))

        try:
            cls = list(map(self._parse_brightness_and_color, args))
        except Exception  as e:
            print(e.args)
            raise ParserError("Error in the colors list: %s"%(
                    ":".join(args)))

        for i in range(len(cls)-1):
            if cls[i + 1][0] <= cls[i][0]:
                raise ParserError("Error in the weights ordering of the colors: %s"%(
                    ":".join(args)))

        if cls[-1][0] != 100 or (len(cls) > 1 and cls[0][0] != 0):
            raise ParserError("Error in the weights of the colors: %s"%(
                    ":".join(args)))

        return cls

    def _parse_brightness_and_color(self, c):
        try:
            if c.startswith("x") and len(c) == 9:
                br = self._parse_num(int(c[1:3], 16), 0, 100)
                rr = self._parse_num(int(c[3:5], 16), 0, 255)
                gg = self._parse_num(int(c[5:7], 16), 0, 255)
                bb = self._parse_num(int(c[7:9], 16), 0, 255)
                return (br, rr, gg, bb)
            elif ',' in c:
                l = c.split(",")
                if len(l) != 4:
                    raise ParserError("Cannot accept '%s' as brightness+color"%(c))
                br = self._parse_num(l[0], 0, 100)
                rr = self._parse_num(l[1], 0, 255)
                gg = self._parse_num(l[2], 0, 255)
                bb = self._parse_num(l[3], 0, 255)
                return (br, rr, gg, bb)
            else:
                assert(0)
        except:
            raise ParserError("Cannot accept '%s' as brightness+color"%(c))

    def _parse_color(self, c):
        try:
            if c.startswith("x") and len(c) == 7:
                rr = self._parse_num(int(c[1:3], 16), 0, 255)
                gg = self._parse_num(int(c[3:5], 16), 0, 255)
                bb = self._parse_num(int(c[5:7], 16), 0, 255)
                return (rr, gg, bb)
            elif ',' in c:
                l = c.split(",")
                if len(l) != 3:
                    raise ParserError("Cannot accept '%s' as color"%(c))
                rr = self._parse_num(l[0], 0, 255)
                gg = self._parse_num(l[1], 0, 255)
                bb = self._parse_num(l[2], 0, 255)
                return (rr, gg, bb)
            else:
                assert(0)
        except ParserError as pe:
            raise ParserError("Cannot accept '%s' as color (%s)"%(c, pe.args[0]))

    def _help(self):
        print("""
usage: atufk7 cmd [cmd [cmd ... ]]
where cmd is:
    set-default-color <color>             set a default colot for all keys

    set-profile <nr>                      set the active profile (nr=0..3)
                                          nr=0 -> default profile

    set-color <key>[,<key>..] <color>     set the color of specific 'key[s]'

    set-breather-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]]
        <mode>          single|double|random
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <c1>,<c2>       color

    set-reactive-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]]
        <mode>          single|double|random
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <c1>,<c2>       color

    set-color-cycle-effect <key>[,<key>..] <speed>:<br>
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100

    set-starry-night-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
        <mode>          single|double|random
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <c1>,<c2>       color
        <c3>            default color (default: 0,0,0)

    set-current-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
        <mode>          single|double|random
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <c1>,<c2>       color
        <c3>            default color (default: 0,0,0)

    set-rain-effect <key>[,<key>..] <mode>:<speed>:<br>[:<c1>[:<c2>]][:<c3>]
        <mode>          single|double|random
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <c1>,<c2>       color
        <c3>            default color (default: 0,0,0)

    set-quicksand-effect <key>[,<key>..] <speed>:<br>:<dir>[c1:c2:..:c6]
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <dir>           up|down
        <c1>,<c2>..<c6> 6 colors (or default if omitted)

    set-wave-effect <key>[,<key>..] <speed>:<br>:<dir>:<thk>[p1,c1:..:pn,cn]
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <dir>           direction: right,bottom-right,bottom,bottom-left,
                        left,upper-left,upper,upper-right
        <thk>           thickness: thick,normal,slim
        <p1,c1>..<pn,cn> colors with position (or default if omitted)
                        position between [0..100]

    set-ripple-effect <key>[,<key>..] <speed>:<br>:<thk>[p1,c1:..:pn,cn]
        <speed>         slow|normal|fast
        <br>            brightness: 0|25|50|75|100
        <thk>           thickness: thick,normal,slim
        <p1,c1>..<pn,cn> colors with position (or default if omitted)
                        position between [0..100]

    get-version         show the keyboard version
    get-layout          show the keyboard layout

    --debug             enable the log of the data transfert

    help|--help         show this page
    help-keys           show the keys name
    show-keys           show one key at time to know the name

    a color may be expressed as:
        xbrrrggbb          where br rr gg bb are in hex format
        br,rr,gg,bb        where br rr gg bb are in decimal format
""")

    def parse(self):
        if len(self._args) == 0:
            self._help()
            sys.exit()

        i = 0
        effects = set()
        while i < len(self._args):
            arg = self._args[i]
            try:
                if arg == "set-default-color":
                    assert (i < len(self._args) -1)
                    self._set_default_color(self._args[i+1])
                    i += 2
                elif arg == "set-color":
                    assert (i < len(self._args) -2)
                    self._set_color(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-breather-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_breather_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-reactive-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_reactive_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-color-cycle-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_color_cycle_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-starry-night-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_starry_night_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-current-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_current_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-rain-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_rain_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-quicksand-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_quicksand_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-wave-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_wave_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-ripple-effect":
                    assert(not arg in effects)
                    effects.add(arg)
                    assert (i < len(self._args) -2)
                    self._set_ripple_effect(self._args[i+1], self._args[i+2])
                    i += 3
                elif arg == "set-profile":
                    assert (i < len(self._args) -1)
                    self._profile = int(self._args[i+1])
                    assert(self._profile >= 0 and self._profile <= 3)
                    i += 2
                elif arg == "help" or arg == "--help":
                    i += 1
                    self._help()
                    sys.exit()
                elif arg == "help-keys":
                    self._help_keys()
                    i += 1
                    sys.exit()
                elif arg == "show-keys":
                    self._show_keys()
                    i += 1
                    sys.exit()
                elif arg == "--debug":
                    self._debug = True
                    i += 1
                elif arg == "get-layout":
                    self._get_layout = True
                    i += 1
                elif arg == "get-version":
                    self._get_version = True
                    i += 1
                else:
                    print("Unknown command '%s'\n"%(arg))
                    print("help for more information")
                    sys.exit()

            except ParserError as pe:
                raise ParserError("Error parsing argument of command '%s'"%(arg),
                        pe.args[0])

    def test_get_version(self):
        return self._get_version
    def test_get_layout(self):
        return self._get_layout
    def test_debug(self):
        return self._debug

    def _help_keys(self):
        print("""The available keys name are:
\t- 0 ... 9         for the digits
\t- A ... Z         for the simple letter""")
        l = []
        for (nr, key) in keyboard:
            if len(key) >= 2:
                l.append(key)
        l.sort()
        for key in l:
            print("\t-", key)

    def _show_keys(self):
        a = AsusKbd()
        for (nr, key) in keyboard:
            if len(key) == 0:
                continue

            print("Key '%s'"%(key))
            keys = self._keys.copy()
            keys[nr] = ('static', (255, 255, 255, 255))
            a.set_keys_color(keys, self._profile)
            print("Press ENTER to continue", end="")
            sys.stdin.readline()
            print("")

    def get_keys(self):
        return self._keys

    def get_profile(self):
        return self._profile


def main():

    pcl = ParseCMDLine(sys.argv[1:])
    try:
        pcl.parse()

        a = AsusKbd()

        a.set_debug(pcl.test_debug())

        if pcl.test_get_version():
            print("Version:", a.get_version())
        if pcl.test_get_layout():
            print("Layout:", a.get_layout())

        keys = pcl.get_keys()
        profile = pcl.get_profile()
        #print("keys=", keys, len(keys))
        if not profile is None:
            a.set_profile(profile)
        if keys:
            a.set_keys_color(keys)

    except ParserError as pe:
        print(pe.args[0])
        print(pe.args[1])


main()





